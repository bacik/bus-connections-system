package busconnectionssystem;

import org.assertj.swing.fixture.FrameFixture;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DatabaseLoginDatabaseLoginUITest extends AbstractDatabaseLoginUITest {

    @Test
    public void inputCorrectLoginInformation() {
        window.textBox("loginTextField").setText("sa");
        window.textBox("passwordTextField").setText("sa");
        window.button("loginButton").click();
        FrameFixture busSystemFrame = new FrameFixture(this.robot(), databaseLoginUI.getBusSystemUI());
        String s = busSystemFrame.target().getTitle();
        assertEquals("Bus Connections System", s);
    }

    @Test
    public void inputWrongLoginInformation() {
        window.textBox("loginTextField").setText("root");
        window.textBox("passwordTextField").setText("root");
        window.button("loginButton").click();
        assertEquals("Connection Error", window.dialog().target().getTitle());
    }

    @Test
    public void resetBothTextFields() {
        window.textBox("loginTextField").setText("root");
        window.textBox("passwordTextField").setText("root");
        window.button("resetButton").click();
        assertEquals("", window.textBox("loginTextField").text());
        assertEquals("", window.textBox("passwordTextField").text());
    }

    @Test
    public void resetOnlyLoginTextField() {
        window.textBox("loginTextField").setText("root");
        window.button("resetButton").click();
        assertEquals("", window.textBox("loginTextField").text());
    }

    @Test
    public void resetOnlyPasswordTextField() {
        window.textBox("passwordTextField").setText("root");
        window.button("resetButton").click();
        assertEquals("", window.textBox("passwordTextField").text());
    }

    @Override
    protected void onTearDown() {
        SqlConnection.setInstance(null);
    }
}