package busconnectionssystem;

import org.junit.AfterClass;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class BusSystemUIBusManagementTest extends AbstractBusSystemUITest {

    @AfterClass
    public static void afterClassTearDown() throws SQLException {
        String sql = "drop all objects";
        PreparedStatement preparedStatement = sqlConnection.getConnection().prepareStatement(sql);
        preparedStatement.executeUpdate();
        SqlConnection.setInstance(null);
    }

    @Override
    protected void onSetUp() {
        window.tabbedPane("tabbedPane1").selectTab("Bus Management");
    }

    @Test
    public void resetAllTextFieldsTest() {
        window.textBox("busIdField").setText("Test");
        window.textBox("busNameField").setText("Test");
        window.textBox("sourceField").setText("Test");
        window.textBox("destinationField").setText("Test");
        window.textBox("dateField").setText("Test");
        window.button("resetButtonInBusManagement").click();
        assertEquals("", window.textBox("busIdField").target().getText());
        assertEquals("", window.textBox("busNameField").target().getText());
        assertEquals("", window.textBox("sourceField").target().getText());
        assertEquals("", window.textBox("destinationField").target().getText());
        assertEquals("", window.textBox("dateField").target().getText());
    }

    @Test
    public void resetBusIDTextFieldTest() {
        window.textBox("busIdField").setText("Test");
        window.button("resetButtonInBusManagement").click();
        assertEquals("", window.textBox("busIdField").target().getText());
    }

    @Test
    public void resetBusNameTextFieldTest() {
        window.textBox("busNameField").setText("Test");
        window.button("resetButtonInBusManagement").click();
        assertEquals("", window.textBox("busNameField").target().getText());
    }

    @Test
    public void resetSourceTextFieldTest() {
        window.textBox("sourceField").setText("Test");
        window.button("resetButtonInBusManagement").click();
        assertEquals("", window.textBox("sourceField").target().getText());
    }

    @Test
    public void resetDestinationTextFieldTest() {
        window.textBox("destinationField").setText("Test");
        window.button("resetButtonInBusManagement").click();
        assertEquals("", window.textBox("destinationField").target().getText());
    }

    @Test
    public void resetDateTextFieldTest() {
        window.textBox("dateField").setText("Test");
        window.button("resetButtonInBusManagement").click();
        assertEquals("", window.textBox("dateField").target().getText());
    }

    @Test
    public void addBusConnectionTest() {
        window.textBox("busIdField").setText("50");
        window.textBox("busNameField").setText("Test");
        window.textBox("sourceField").setText("Brno");
        window.textBox("destinationField").setText("Praha");
        window.textBox("dateField").setText("2020-09-09 10:00");
        window.button("addBusButton").click();
        assertEquals("Bus added", window.dialog().target().getTitle());
    }

    @Test
    public void addBusConnectionWithoutIDTest() {
        window.textBox("busNameField").setText("Test");
        window.textBox("sourceField").setText("Brno");
        window.textBox("destinationField").setText("Praha");
        window.textBox("dateField").setText("2020-09-09 10:00");
        window.button("addBusButton").click();
        assertEquals("Bus added", window.dialog().target().getTitle());
    }

    @Test
    public void addBusConnectionDuplicateTest() {
        window.textBox("busIdField").setText("3");
        window.textBox("busNameField").setText("Test");
        window.textBox("sourceField").setText("Brno");
        window.textBox("destinationField").setText("Praha");
        window.textBox("dateField").setText("2020-09-09 10:00");
        window.button("addBusButton").click();
        assertEquals("Duplicate found", window.dialog().target().getTitle());
    }

    @Test
    public void addBusConnectionNotEnoughInformationTest() {
        window.textBox("busIdField").setText("50");
        window.textBox("busNameField").setText("Test");
        window.textBox("sourceField").setText("Brno");
        window.textBox("destinationField").setText("Praha");
        window.button("addBusButton").click();
        assertEquals("Not enough information", window.dialog().target().getTitle());
    }

    @Test
    public void removeBusConnectionTest() {
        window.textBox("busIdField").setText("1");
        window.textBox("busNameField").setText("AB01");
        window.textBox("sourceField").setText("Presov");
        window.textBox("destinationField").setText("Poprad");
        window.textBox("dateField").setText("2020-09-01 13:00");
        window.button("removeBusButton").click();
        assertEquals("Bus removed", window.dialog().target().getTitle());
    }

    @Test
    public void removeBusConnectionWithoutIDTest() {
        window.textBox("busNameField").setText("AB02");
        window.textBox("sourceField").setText("Presov");
        window.textBox("destinationField").setText("Poprad");
        window.textBox("dateField").setText("2020-09-02 13:00");
        window.button("removeBusButton").click();
        assertEquals("Bus removed", window.dialog().target().getTitle());
    }

    @Test
    public void removeBusConnectionDoesNotExistTest() {
        window.textBox("busIdField").setText("50");
        window.textBox("busNameField").setText("Test");
        window.textBox("sourceField").setText("Brno");
        window.textBox("destinationField").setText("Praha");
        window.textBox("dateField").setText("2020-09-01 13:00");
        window.button("removeBusButton").click();
        assertEquals("No bus found", window.dialog().target().getTitle());
    }

    @Test
    public void removeBusConnectionNotEnoughInformationTest() {
        window.textBox("busIdField").setText("50");
        window.textBox("busNameField").setText("Test");
        window.textBox("sourceField").setText("Brno");
        window.textBox("destinationField").setText("Praha");
        window.button("removeBusButton").click();
        assertEquals("Not enough information", window.dialog().target().getTitle());
    }
}