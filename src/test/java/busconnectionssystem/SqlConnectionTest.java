package busconnectionssystem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SqlConnectionTest {
    private SqlConnection sqlConnection;

    public void addOneLine() throws SQLException {
        sqlConnection.addBusConnectionToDB("", "AB01", "Presov", "Poprad", "2020-09-20 13:00");
    }

    public ResultSet selectAllConnections() throws SQLException {
        String sql = "select * from connections";
        PreparedStatement preparedStatement = sqlConnection.getConnection().prepareStatement(sql);
        return preparedStatement.executeQuery();
    }

    @Before
    public void setup() throws SQLException {
        SqlConnection.setDatabaseType("org.h2.Driver");
        SqlConnection.setDatabaseURL("jdbc:h2:~/test");
        SqlConnection.setLogin("sa");
        SqlConnection.setPassword("sa");
        sqlConnection = SqlConnection.getInstance();

        String sql = "CREATE TABLE `connections` (\n" +
                "  `bus_id` int NOT NULL AUTO_INCREMENT,\n" +
                "  `name` varchar(45) NOT NULL,\n" +
                "  `source` varchar(45) NOT NULL,\n" +
                "  `destination` varchar(45) NOT NULL,\n" +
                "  `time` datetime NOT NULL,\n" +
                "  PRIMARY KEY (`bus_id`)\n" +
                ")";
        PreparedStatement preparedStatement = sqlConnection.getConnection().prepareStatement(sql);
        preparedStatement.executeUpdate();
    }

    @After
    public void teardown() throws SQLException {
        String sql = "drop all objects";
        PreparedStatement preparedStatement = sqlConnection.getConnection().prepareStatement(sql);
        preparedStatement.executeUpdate();
        SqlConnection.setInstance(null);
    }

    @Test
    public void invalidSQLConnection() {
        SqlConnection.setDatabaseType("");
        SqlConnection.setDatabaseURL("");
        SqlConnection.setLogin("");
        SqlConnection.setPassword("");
        try {
            sqlConnection = SqlConnection.getInstance();
        } catch (SQLException e) {
            assertNull(sqlConnection.getConnection());
        }
    }

    @Test
    public void addBusConnectionToDBWithEmptyIDTest() throws SQLException {
        addOneLine();
        ResultSet resultSet = selectAllConnections();
        resultSet.next();
        assertEquals("1", resultSet.getString(1));
        assertEquals("AB01", resultSet.getString(2));
    }

    @Test
    public void addBusConnectionToDBWithIDTest() throws SQLException {
        sqlConnection.addBusConnectionToDB("5", "AB01", "Presov", "Poprad", "2020-09-20 13:00");
        ResultSet resultSet = selectAllConnections();
        resultSet.next();
        assertEquals("5", resultSet.getString(1));
        assertEquals("AB01", resultSet.getString(2));
    }

    @Test
    public void removeBusConnectionFromDBWithEmptyIDTest() throws SQLException {
        addOneLine();
        ResultSet resultSet = selectAllConnections();
        resultSet.next();
        assertEquals("1", resultSet.getString(1));
        sqlConnection.removeBusConnectionFromDB("", "AB01", "Presov", "Poprad", "2020-09-20 13:00");
        ResultSet resultSetAfterRemoval = selectAllConnections();
        assertFalse(resultSetAfterRemoval.next());
    }

    @Test
    public void removeBusConnectionFromDBWithIDTest() throws SQLException {
        addOneLine();
        ResultSet resultSet = selectAllConnections();
        resultSet.next();
        assertEquals("1", resultSet.getString(1));
        sqlConnection.removeBusConnectionFromDB("1", "AB01", "Presov", "Poprad", "2020-09-20 13:00");
        ResultSet resultSetAfterRemoval = selectAllConnections();
        assertFalse(resultSetAfterRemoval.next());
    }

    @Test
    public void removalSuccessfulTest() throws SQLException {
        addOneLine();
        ResultSet resultSet = selectAllConnections();
        resultSet.next();
        assertEquals("1", resultSet.getString(1));
        sqlConnection.removeBusConnectionFromDB("1", "AB01", "Presov", "Poprad", "2020-09-20 13:00");
        assertEquals(1, sqlConnection.getRemovalSuccessful());
    }

    @Test
    public void getSourcesFromDBTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Trnava", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.getSourcesFromDB();
        List<String> listOfSources = new ArrayList<>();
        while (resultSet.next()) {
            listOfSources.add(resultSet.getString(1));
        }
        assertTrue(listOfSources.contains("Poprad") && listOfSources.contains("Nitra") && listOfSources.contains("Presov"));
    }

    @Test
    public void getSourcesFromDBDistinctTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Poprad", "Trnava", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.getSourcesFromDB();
        List<String> listOfSources = new ArrayList<>();
        while (resultSet.next()) {
            listOfSources.add(resultSet.getString(1));
        }
        assertTrue(listOfSources.contains("Poprad") && listOfSources.contains("Presov") && listOfSources.size() == 2);
    }

    @Test
    public void getDestinationsFromDBTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Trnava", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.getDestinationsFromDB();
        List<String> listOfSources = new ArrayList<>();
        while (resultSet.next()) {
            listOfSources.add(resultSet.getString(1));
        }
        assertTrue(listOfSources.contains("Poprad") && listOfSources.contains("Kosice") && listOfSources.contains("Trnava"));
    }

    @Test
    public void getDestinationsFromDBDistinctTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.getDestinationsFromDB();
        List<String> listOfSources = new ArrayList<>();
        while (resultSet.next()) {
            listOfSources.add(resultSet.getString(1));
        }
        assertTrue(listOfSources.contains("Kosice") && listOfSources.contains("Poprad") && listOfSources.size() == 2);
    }

    @Test
    public void searchForBusConnectionsInDBAllInBothTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(10, "All", "All", "", "");
        int counter = 0;
        while (resultSet.next()) {
            counter++;
        }
        assertEquals(3, counter);
    }

    @Test
    public void searchForBusConnectionsInDBAllInSourceTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(10, "All", "Poprad", "", "");
        int counter = 0;
        while (resultSet.next()) {
            counter++;
        }
        assertEquals(1, counter);
    }

    @Test
    public void searchForBusConnectionsInDBAllInDestinationTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(10, "Poprad", "All", "", "");
        int counter = 0;
        while (resultSet.next()) {
            counter++;
        }
        assertEquals(1, counter);
    }

    @Test
    public void searchForBusConnectionsInDBParticularConnectionTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(10, "Poprad", "Kosice", "", "");
        int counter = 0;
        while (resultSet.next()) {
            counter++;
        }
        assertEquals(1, counter);
    }

    @Test
    public void searchForBusConnectionsInDBLimitTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(1, "All", "All", "", "");
        int counter = 0;
        while (resultSet.next()) {
            counter++;
        }
        assertEquals(1, counter);
    }

    @Test
    public void searchForBusConnectionsInDBDateTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(10, "All", "All", "09-20", "");
        int counter = 0;
        while (resultSet.next()) {
            counter++;
        }
        assertEquals(3, counter);
    }

    @Test
    public void searchForBusConnectionsInDBDateNegativeTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(10, "All", "All", "09-22", "");
        assertFalse(resultSet.next());
    }

    @Test
    public void searchForBusConnectionsInDBTimeTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(10, "All", "All", "", "14");
        int counter = 0;
        while (resultSet.next()) {
            counter++;
        }
        assertEquals(1, counter);
    }

    @Test
    public void searchForBusConnectionsInDBTimeNegativeTest() throws SQLException {
        addOneLine();
        sqlConnection.addBusConnectionToDB("", "AB02", "Poprad", "Kosice", "2020-09-20 14:00");
        sqlConnection.addBusConnectionToDB("", "AB03", "Nitra", "Kosice", "2020-09-20 15:00");
        ResultSet resultSet = sqlConnection.searchForBusConnectionsInDB(10, "All", "All", "", "16");
        assertFalse(resultSet.next());
    }
}