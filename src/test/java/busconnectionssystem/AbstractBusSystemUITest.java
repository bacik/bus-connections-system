package busconnectionssystem;

import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.edt.GuiQuery;
import org.assertj.swing.fixture.FrameFixture;
import org.assertj.swing.testing.AssertJSwingTestCaseTemplate;
import org.junit.*;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AbstractBusSystemUITest extends AssertJSwingTestCaseTemplate {
    protected static SqlConnection sqlConnection;
    protected BusSystemUI BusSystemUI;
    protected FrameFixture window;

    @BeforeClass
    public static void setUpOnce() throws SQLException {
        FailOnThreadViolationRepaintManager.install();
        SqlConnection.setDatabaseType("org.h2.Driver");
        SqlConnection.setDatabaseURL("jdbc:h2:~/test");
        SqlConnection.setLogin("sa");
        SqlConnection.setPassword("sa");
        sqlConnection = SqlConnection.getInstance();

        String sql = "CREATE TABLE `connections` (\n" +
                "  `bus_id` int NOT NULL AUTO_INCREMENT,\n" +
                "  `name` varchar(45) NOT NULL,\n" +
                "  `source` varchar(45) NOT NULL,\n" +
                "  `destination` varchar(45) NOT NULL,\n" +
                "  `time` datetime NOT NULL,\n" +
                "  PRIMARY KEY (`bus_id`)\n" +
                ")";
        PreparedStatement preparedStatement = sqlConnection.getConnection().prepareStatement(sql);
        preparedStatement.executeUpdate();
        addMultipleBusConnections();
    }

    @Before
    public void setUp() {
        this.setUpRobot();

        BusSystemUI = GuiActionRunner.execute(new GuiQuery<>() {
            @Override
            protected BusSystemUI executeInEDT() throws SQLException {
                return new BusSystemUI();
            }
        });
        this.window = new FrameFixture(this.robot(), BusSystemUI);
        this.window.show();
        onSetUp();
    }

    public static void addMultipleBusConnections() throws SQLException {
        for (int i = 1; i < 22; i++) {
            if (i < 10) {
                sqlConnection.addBusConnectionToDB("", "AB0" + i, "Presov", "Poprad", "2020-09-0" + i + " 13:00");
            } else {
                sqlConnection.addBusConnectionToDB("", "AB" + i, "Presov", "Poprad", "2020-09-" + i + " 13:00");
            }
        }
    }

    protected void onSetUp() {
    }

    @After
    public final void tearDown() {
        try {
            onTearDown();
            this.window = null;
        } finally {
            cleanUp();
        }
    }

    protected void onTearDown() {
    }
}