package busconnectionssystem;

import org.assertj.swing.edt.FailOnThreadViolationRepaintManager;
import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.edt.GuiQuery;
import org.assertj.swing.fixture.FrameFixture;

import org.assertj.swing.testing.AssertJSwingTestCaseTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;


public class AbstractDatabaseLoginUITest extends AssertJSwingTestCaseTemplate {
    protected FrameFixture window;
    protected DatabaseLoginUI databaseLoginUI;

    @BeforeClass
    public static void setUpOnce() {
        FailOnThreadViolationRepaintManager.install();
    }

    @Before
    public final void setUp() {
        this.setUpRobot();
        SqlConnection.setDatabaseType("org.h2.Driver");
        SqlConnection.setDatabaseURL("jdbc:h2:~/test");

        databaseLoginUI = GuiActionRunner.execute(new GuiQuery<>() {
            @Override
            protected DatabaseLoginUI executeInEDT() {
                return new DatabaseLoginUI();
            }
        });
        this.window = new FrameFixture(this.robot(), databaseLoginUI);
        this.window.show();
        onSetUp();
    }

    protected void onSetUp() {
    }

    @After
    public final void tearDown() {
        try {
            onTearDown();
            this.window = null;
        } finally {
            cleanUp();
        }
    }

    protected void onTearDown() {
    }
}