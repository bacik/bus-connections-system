package busconnectionssystem;

import org.junit.AfterClass;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class BusSystemUIConnectionsTest extends AbstractBusSystemUITest {

    @AfterClass
    public static void afterClassTearDown() throws SQLException {
        String sql = "drop all objects";
        PreparedStatement preparedStatement = sqlConnection.getConnection().prepareStatement(sql);
        preparedStatement.executeUpdate();
        SqlConnection.setInstance(null);
    }

    @Test
    public void searchLimitSetToTenTest() {
        window.button("searchButton").click();
        assertEquals(10, window.table("table1").target().getRowCount());
    }

    @Test
    public void searchLimitSetToTwentyTest() {
        window.comboBox("limitComboBox").selectItem("20");
        window.button("searchButton").click();
        assertEquals(20, window.table("table1").target().getRowCount());
    }

    @Test
    public void searchLimitSetToFiftyTest() {
        window.comboBox("limitComboBox").selectItem("50");
        window.button("searchButton").click();
        assertEquals(21, window.table("table1").target().getRowCount());
    }

    @Test
    public void searchSpecificFromTest() {
        window.comboBox("fromComboBox").selectItem("Presov");
        window.comboBox("limitComboBox").selectItem("50");
        window.button("searchButton").click();
        assertEquals(21, window.table("table1").target().getRowCount());
    }

    @Test
    public void searchSpecificToTest() {
        window.comboBox("toComboBox").selectItem("Poprad");
        window.comboBox("limitComboBox").selectItem("50");
        window.button("searchButton").click();
        assertEquals(21, window.table("table1").target().getRowCount());
    }

    @Test
    public void searchSpecificDayTest() {
        window.textBox("dayTextField").setText("2020-09-01");
        window.comboBox("limitComboBox").selectItem("50");
        window.button("searchButton").click();
        assertEquals(1, window.table("table1").target().getRowCount());
    }

    @Test
    public void searchSpecificTimeTest() {
        window.textBox("timeTextField").setText("13:00");
        window.comboBox("limitComboBox").selectItem("50");
        window.button("searchButton").click();
        assertEquals(21, window.table("table1").target().getRowCount());
    }

    @Test
    public void searchOneSpecificBusConnectionTest() {
        window.comboBox("fromComboBox").selectItem("Presov");
        window.comboBox("toComboBox").selectItem("Poprad");
        window.textBox("dayTextField").setText("2020-09-01");
        window.textBox("timeTextField").setText("13:00");
        window.button("searchButton").click();
        assertEquals("Poprad", window.table("table1").target().getValueAt(0, 3));
        assertEquals(1, window.table("table1").target().getRowCount());
    }

    @Test
    public void searchOneSpecificBusConnectionNegativeTest() {
        window.comboBox("fromComboBox").selectItem("Presov");
        window.comboBox("toComboBox").selectItem("Poprad");
        window.textBox("dayTextField").setText("2000-01-01");
        window.button("searchButton").click();
        assertEquals("Nothing found", window.dialog().target().getTitle());
    }

    @Test
    public void searchResetAllTest() {
        window.comboBox("fromComboBox").selectItem("Presov");
        window.comboBox("toComboBox").selectItem("Poprad");
        window.textBox("dayTextField").setText("2020-09-01");
        window.textBox("timeTextField").setText("13:00");
        window.button("resetButtonInConnections").click();
        assertEquals("All", window.comboBox("fromComboBox").target().getSelectedItem());
        assertEquals("All", window.comboBox("toComboBox").target().getSelectedItem());
        assertEquals("", window.textBox("dayTextField").target().getText());
        assertEquals("", window.textBox("timeTextField").target().getText());
    }

    @Test
    public void searchResetFromTest() {
        window.comboBox("fromComboBox").selectItem("Presov");
        window.button("resetButtonInConnections").click();
        assertEquals("All", window.comboBox("fromComboBox").target().getSelectedItem());
    }

    @Test
    public void searchResetToTest() {
        window.comboBox("toComboBox").selectItem("Poprad");
        window.button("resetButtonInConnections").click();
        assertEquals("All", window.comboBox("toComboBox").target().getSelectedItem());
    }

    @Test
    public void searchResetDayTest() {
        window.textBox("dayTextField").setText("2020-09-01");
        window.button("resetButtonInConnections").click();
        assertEquals("", window.textBox("dayTextField").target().getText());
    }

    @Test
    public void searchResetTimeTest() {
        window.textBox("timeTextField").setText("13:00");
        window.button("resetButtonInConnections").click();
        assertEquals("", window.textBox("timeTextField").target().getText());
    }
}