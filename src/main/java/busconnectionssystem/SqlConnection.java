package busconnectionssystem;

import java.sql.*;

public class SqlConnection {
    private static String login;
    private static String password;
    private static SqlConnection instance;
    private Connection connection;
    private static String databaseURL = "jdbc:mysql://localhost:3306/busreservation";
    private static String databaseType = "com.mysql.cj.jdbc.Driver";
    private int removalSuccessful;

    public int getRemovalSuccessful() {
        return removalSuccessful;
    }

    private SqlConnection() throws SQLException {
        try {
            Class.forName(databaseType);
            this.connection = DriverManager.getConnection(databaseURL, login, password);
        } catch (ClassNotFoundException ex) {
            System.out.println("Database Connection Creation Failed : " + ex.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public static void setInstance(SqlConnection instance) {
        SqlConnection.instance = instance;
    }

    public static SqlConnection getInstance() throws SQLException {
        if (instance == null || instance.getConnection().isClosed()) {
            instance = new SqlConnection();
        }
        return instance;
    }

    public static void setLogin(String log) {
        login = log;
    }

    public static void setPassword(String pwd) {
        password = pwd;
    }

    public static void setDatabaseURL(String databaseURL) {
        SqlConnection.databaseURL = databaseURL;
    }

    public static void setDatabaseType(String databaseType) {
        SqlConnection.databaseType = databaseType;
    }

    public ResultSet getSourcesFromDB() {
        try {
            String distinctSourcesQuery = "select distinct source from connections";
            PreparedStatement populateSourceStatement = getConnection().prepareStatement(distinctSourcesQuery);
            return populateSourceStatement.executeQuery();
        } catch (SQLException e) {
            return null;
        }
    }

    public ResultSet getDestinationsFromDB() {
        try {
            String distinctDestinationsQuery = "select distinct destination from connections";
            PreparedStatement populateDestinationStatement = getConnection().prepareStatement(distinctDestinationsQuery);
            return populateDestinationStatement.executeQuery();
        } catch (SQLException e) {
            return null;
        }
    }

    public ResultSet searchForBusConnectionsInDB(int limit, String source, String destination, String date, String time) {
        if (source.equals("All")) {
            source = "%";
        }
        if (destination.equals("All")) {
            destination = "%";
        }
        String busSearch = "select * from connections where source like '" + source + "' and destination like '" + destination + "' and time like '%" + date + "%" + time + "%'" + " limit " + limit;
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(busSearch);

            return preparedStatement.executeQuery();
        } catch (SQLException e) {
            return null;
        }
    }

    public void addBusConnectionToDB(String busID, String name, String source, String destination, String timeDate) throws SQLException {
        String additionToDatabase = "insert into connections(bus_id, name, source, destination, time) values (";

        if (busID.isEmpty()) {
            additionToDatabase += "NULL," + "'" + name + "'" + "," + "'" + source + "'" + "," + "'" + destination + "'" + "," + "'" + timeDate + "'" + ");";
        } else {
            additionToDatabase += "" + busID + "," + "'" + name + "'" + "," + "'" + source + "'" + "," + "'" + destination + "'" + "," + "'" + timeDate + "'" + ")";
        }
        PreparedStatement additionStatement = getConnection().prepareStatement(additionToDatabase);
        additionStatement.executeUpdate();
    }

    public void removeBusConnectionFromDB(String busID, String name, String source, String destination, String timeDate) {
        String removeInfo = "delete from connections where ";

        if (busID.isEmpty()) {
            removeInfo += "name =" + "'" + name + "'" + "and source=" + "'" + source + "'" + "and destination=" + "'" + destination + "'" + "and time=" + "'" + timeDate + "'" + ";";
        } else {
            removeInfo += "bus_id =" + busID + "and name =" + "'" + name + "'" + "and source=" + "'" + source + "'" + "and destination=" + "'" + destination + "'" + "and time=" + "'" + timeDate + "'" + ";";
        }
        try {
            PreparedStatement removalStatement = getConnection().prepareStatement(removeInfo);
            removalSuccessful = removalStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error removing from the database.");
        }
    }
}