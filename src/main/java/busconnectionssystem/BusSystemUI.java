package busconnectionssystem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.*;

public class BusSystemUI extends JFrame {
    private DefaultTableModel model = new DefaultTableModel(new String[]{"Bus ID", "Bus Name", "Source", "Destination", "Date"}, 0);
    private JTextArea busConnectionsSystemTextArea;
    private JTabbedPane tabbedPane1;
    private JPanel reservationPanel;
    private JComboBox<String> fromComboBox;
    private JTextPane fromTextPane;
    private JTextPane toTextPane;
    private JComboBox<String> toComboBox;
    private JButton searchButton;
    private JTextField destinationField;
    private JTextPane busIDTextPane;
    private JTextField busIdField;
    private JTextField busNameField;
    private JTextField sourceField;
    private JButton addBusButton;
    private JButton removeBusButton;
    private JTextField dateField;
    private JTextField dayTextField;
    private JTextField timeTextField;
    private JTable table1;
    private JButton resetButtonInConnections;
    private JTextPane dayTextPane;
    private JTextPane timeTextPane;
    private JComboBox<Integer> limitComboBox;
    private JButton resetButtonInBusManagement;
    private JTextPane textPane1;

    public BusSystemUI() throws SQLException {
        super("Bus Connections System");
        setNamesOfComponents();
        this.setContentPane(reservationPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();

        final SqlConnection databaseConnection = SqlConnection.getInstance();
        populateSourcesInSearch(databaseConnection.getSourcesFromDB());
        populateDestinationsInSearch(databaseConnection.getDestinationsFromDB());
        addLimitValuesToSearch();

        this.setSize(625, 400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        table1.setDefaultEditor(Object.class, null);
        searchButton.addActionListener(e -> {
            try {
                searchForBusConnectionsInDB();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        resetButtonInConnections.addActionListener(e -> resetFieldInSearch());
        addBusButton.addActionListener(e -> {
            try {
                addBusConnectionToDB();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        removeBusButton.addActionListener(e -> {
            try {
                removeBusConnectionFromDB();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
        resetButtonInBusManagement.addActionListener(e -> resetFieldsInBusManagement());
    }

    public void searchForBusConnectionsInDB() throws SQLException {
        model.setRowCount(0);
        SqlConnection databaseConnection = SqlConnection.getInstance();
        populateSearchInDBTable(databaseConnection.searchForBusConnectionsInDB(Integer.parseInt(limitComboBox.getSelectedItem().toString()), fromComboBox.getSelectedItem().toString(), toComboBox.getSelectedItem().toString(), dayTextField.getText(), timeTextField.getText()));
        table1.setModel(model);
        if (model.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "No bus was found with given search criteria.", "Nothing found", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void addBusConnectionToDB() throws SQLException {
        SqlConnection databaseConnection = SqlConnection.getInstance();
        if (busNameField.getText().isEmpty() || sourceField.getText().isEmpty() || destinationField.getText().isEmpty() || dateField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Not enough information to add to database. Please add information to all mandatory fields.", "Not enough information", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                databaseConnection.addBusConnectionToDB(busIdField.getText(), busNameField.getText(), sourceField.getText(), destinationField.getText(), dateField.getText());
                JOptionPane.showMessageDialog(null, "The bus was successfully added to the database.", "Bus added", JOptionPane.INFORMATION_MESSAGE);
                resetFieldsInBusManagement();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "This Bus ID already exists, please enter another or leave blank.", "Duplicate found", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void removeBusConnectionFromDB() throws SQLException {
        SqlConnection databaseConnection = SqlConnection.getInstance();
        if (busNameField.getText().isEmpty() || sourceField.getText().isEmpty() || destinationField.getText().isEmpty() || dateField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Not enough information to remove from database. Please add information to all mandatory fields.", "Not enough information", JOptionPane.WARNING_MESSAGE);
        } else {
            databaseConnection.removeBusConnectionFromDB(busIdField.getText(), busNameField.getText(), sourceField.getText(), destinationField.getText(), dateField.getText());
            if (databaseConnection.getRemovalSuccessful() == 0) {
                JOptionPane.showMessageDialog(null, "Bus with selected information does not exist in the database.", "No bus found", JOptionPane.WARNING_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "The bus/buses was/were successfully removed from the database.", "Bus removed", JOptionPane.INFORMATION_MESSAGE);
                resetFieldsInBusManagement();
            }
        }
    }

    public void addLimitValuesToSearch() {
        limitComboBox.addItem(10);
        limitComboBox.addItem(20);
        limitComboBox.addItem(50);
    }

    public void resetFieldInSearch() {
        fromComboBox.setSelectedItem("All");
        toComboBox.setSelectedItem("All");
        dayTextField.setText("");
        timeTextField.setText("");
        model.setRowCount(0);
    }

    public void resetFieldsInBusManagement() {
        busIdField.setText("");
        busNameField.setText("");
        sourceField.setText("");
        destinationField.setText("");
        dateField.setText("");
    }

    public void populateSourcesInSearch(ResultSet resultSet) throws SQLException {
        fromComboBox.addItem("All");
        if (resultSet != null) {
            while (resultSet.next()) {
                fromComboBox.addItem(resultSet.getString("source"));
            }
        }
    }

    public void populateDestinationsInSearch(ResultSet resultSet) throws SQLException {
        toComboBox.addItem("All");
        if (resultSet != null) {
            while (resultSet.next()) {
                toComboBox.addItem(resultSet.getString("destination"));
            }
        }
    }

    public void populateSearchInDBTable(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            String id = resultSet.getString(1);
            String name = resultSet.getString(2);
            String from = resultSet.getString(3);
            String to = resultSet.getString(4);
            String timeDate = resultSet.getString(5);
            model.addRow(new Object[]{id, name, from, to, timeDate});
        }
    }

    public void setNamesOfComponents() {
        searchButton.setName("searchButton");
        resetButtonInConnections.setName("resetButtonInConnections");
        fromComboBox.setName("fromComboBox");
        toComboBox.setName("toComboBox");
        dayTextField.setName("dayTextField");
        timeTextField.setName("timeTextField");
        limitComboBox.setName("limitComboBox");
        table1.setName("table1");
        tabbedPane1.setName("tabbedPane1");
        busIdField.setName("busIdField");
        busNameField.setName("busNameField");
        sourceField.setName("sourceField");
        destinationField.setName("destinationField");
        dateField.setName("dateField");
        addBusButton.setName("addBusButton");
        removeBusButton.setName("removeBusButton");
        resetButtonInBusManagement.setName("resetButtonInBusManagement");
    }
}