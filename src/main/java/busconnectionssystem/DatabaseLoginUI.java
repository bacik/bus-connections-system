package busconnectionssystem;

import javax.swing.*;
import java.sql.*;

public class DatabaseLoginUI extends JFrame {
    private String login;
    private char[] password;
    private JTextPane loginTextPane;
    private JPanel loginPanel;
    private JTextPane passwordTextPane;
    private JPasswordField passwordTextField;
    private JTextField loginTextField;
    private JButton loginButton;
    private JButton resetButton;
    private BusSystemUI busSystemUI;

    public String getPassword() {
        StringBuilder temp = new StringBuilder();
        for (char a : password) {
            temp.append(a);
        }
        return temp.toString();
    }

    public BusSystemUI getBusSystemUI() {
        return busSystemUI;
    }

    public DatabaseLoginUI() {
        super("Login to Database");
        setNamesOfComponents();
        this.setContentPane(loginPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getRootPane().setDefaultButton(loginButton);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        loginButton.addActionListener(e -> {
            try {
                login();
            } catch (SQLException | InterruptedException throwables) {
                JOptionPane.showMessageDialog(null, "Failed to connect to the Database. Login or Password is incorrect.", "Connection Error", JOptionPane.WARNING_MESSAGE);
            }
        });
        resetButton.addActionListener(e -> resetFieldsInDBLogin());
    }

    public void login() throws SQLException, InterruptedException {
        login = loginTextField.getText();
        password = passwordTextField.getPassword();
        SqlConnection.setLogin(login);
        SqlConnection.setPassword(getPassword());
        SqlConnection databaseConnection = SqlConnection.getInstance();

        if (databaseConnection.getConnection().isValid(0)) {
            this.dispose();
            Thread.sleep(1000);
            busSystemUI = new BusSystemUI();
        }
    }

    public void resetFieldsInDBLogin() {
        loginTextField.setText("");
        passwordTextField.setText("");
    }

    public void setNamesOfComponents() {
        passwordTextField.setName("passwordTextField");
        loginTextField.setName("loginTextField");
        loginButton.setName("loginButton");
        resetButton.setName("resetButton");
    }
}