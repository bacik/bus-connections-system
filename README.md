### **Bus Connections System**   
Project that connects different skills - **Java, SQL** and a little bit of frontend with **Java Swing**. Tests done using **JUnit** and **AssertJ Swing** libraries connecting to **H2** in-memory database. 
The main purpose of the program is to connect to a local MySQL database that has a table with bus connections to and from different cities (the data is made up). 
Then the search function filters the database entries and shows only the ones the user is searching for. The UI also contains some administrator functions - adding and removing bus connections from the database.
***
Tests completed as of **16th September 2020**. The current functionality includes logging in into a database, searching for bus connections, adding and removing bus connections from the database.  
***
1.05 - Abstract test class AbstractBusSystemUITest added. BusSystemUIBusManagementTest and BusSystemUIConnectionsTest test classes added to test the Bus Management tab functionality.

1.04 - DatabaseLoginUITest, AbstractUITest test classes added - done using **JUnit** and **AssertJ Swing** libraries connecting to **H2** in-memory database.  

1.03 - SqlConnection tests added - done using **JUnit** and **H2** in-memory database.  
 
1.02 - Folder structure refactored, .gitignore file properly applied - files that should not be shared deleted.  

1.01 - Added addition and removal functions under Bus Management.  

P.S.: The program will probably not work on your machine because it is trying to connect to a localhost MySQL database and you need to provide login and password information.